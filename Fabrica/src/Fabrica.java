import javax.swing.JOptionPane;

class Coches{
	
	private String matricula; 
	private String marca;
	private String modelo;
	private String color;
	private boolean techoSolar;
	private double km; 
	private int numPuertas = 3;
	private int numPlazas;
	
	public Coches() {
		
		matricula = "";
		marca = "Seat";
		modelo = "Altea";
		color = "Blanco";
		techoSolar = false;
		numPuertas = 3;
		numPlazas = 5;
		km = 0;	
		
	}
	
	public Coches(String matricula) {
		
		this.matricula = matricula;	
		marca = "Seat";
		modelo = "Altea";
		color = "Blanco";
		techoSolar = false;
		numPuertas = 3;
		numPlazas = 5;
		km = 0;			
	}
	
	public Coches (int numPuertas, int numPlazas) {
		matricula = "";
		marca = "Seat";
		modelo = "Altea";
		color = "Blanco";
		techoSolar = false;
		this.numPuertas = numPuertas;
		this.numPlazas = numPlazas;	
		km = 0;			
	}
	
	public Coches (String marca, String modelo, String color) {
		
		matricula = "";
		this.marca = marca;
		this.modelo = modelo;
		this.color = color;	
		techoSolar = false;
		numPuertas = 3;
		numPlazas = 5;
		km = 0;
	}	
	
	public void avanzar(double km) {
		
		this.km = this.km + km;
		JOptionPane.showMessageDialog(null,"Se han modificado los km a: " + km);
		
	}
	
	public void tunear() {
		
		this.km = 0;
		techoSolar = true;
		JOptionPane.showMessageDialog(null,"Los km se han puesto a 0 y se ha instalado el techo solar");
	}
	
	public void tunear (String color) {
		
		this.color = color;
		this.km = 0;
		this.techoSolar = true;
		JOptionPane.showMessageDialog(null,"Los km se han puesto a 0, se ha instalado el techo solar y se ha cambiado el color");
	}
	
	public void matricular (String matricula) {
		
		this.matricula = matricula;
		JOptionPane.showMessageDialog(null,"La nueva matricula es: " + matricula);		
	}
	
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isTechoSolar() {
		return techoSolar;
	}

	public void setTechoSolar(boolean techoSolar) {
		this.techoSolar = techoSolar;
	}

	public double getKm() {
		return km;
	}

	public void setKm(double km) {
		this.km = km;
	}

	public int getNumPuertas() {
		return numPuertas;
	}

	public void setNumPuertas(int numPuertas) {
		
		if (numPlazas > 0 && numPuertas < 6) {
			this.numPuertas = numPuertas;
		}else {
			
		}	
	}

	public int getNumPlazas() {
		return numPlazas;
	}

	public void setNumPlazas(int numPlazas) {
		
		if (numPlazas > 0 && numPlazas < 8) {
			this.numPlazas = numPlazas;
		}else {
			this.numPlazas = 0;
		}
	}
}

public class Fabrica {

	public static String caracteristica(Coches cars) {
		
		String techo;
		String caracteristicas = null;
		
		if (cars.isTechoSolar() == true) {
			techo = "Si";
		}else {
			techo = "no";
		}
		
		JOptionPane.showMessageDialog(null, "Matricula: " + cars.getMatricula() + "\nMarca: " + cars.getMarca() + "\nModelo: " + cars.getModelo() + "\nColor: " + cars.getColor() + "\nTecho solar: " + techo + "\nKm: " + cars.getKm() + "\nN�mero puertas: " + cars.getNumPuertas() + "\nN�mero plazas :" + cars.getNumPlazas());
		
		return caracteristicas;
	}
	
	public static void main(String[] args) {
			Coches car1;
			Coches car2;
			Coches car3;
			Coches car4;
			
			car1 = new Coches();
			car2 = new Coches();
			car3 = new Coches();
			car4 = new Coches();
			
			car1.matricular("1234-DF");
			car1.setMarca("Seat");
			car1.setModelo("Toledo");
			car1.setColor("Azul");
			car1.setTechoSolar(false);
			car1.setKm(100);
			car1.setNumPuertas(3);
			car1.setNumPlazas(5);
			
			car2.matricular("5678-AG");
			car2.setMarca("Fiat");
			car2.setModelo("Uno");
			car2.setColor("Rojo");
			car2.setTechoSolar(true);
			car2.avanzar(200);
			car2.setNumPuertas(3);
			car2.setNumPlazas(2);
			
			car3.matricular("9012-HH");
			car3.setMarca("BMW");
			car3.setModelo("850");
			car3.setColor("Gris");
			car3.setTechoSolar(false);
			car3.avanzar(300);
			car3.setNumPuertas(5);
			car3.setNumPlazas(5);
			
			car4.matricular("3456-XS");
			car4.setMarca("VW");
			car4.setModelo("Caddy");
			car4.setColor("Blanco");
			car4.setTechoSolar(true);
			car4.avanzar(400);
			car4.setNumPuertas(5);
			car4.setNumPlazas(7);
			
			Coches ArrayCoches[] = new Coches[4]; 
			
			ArrayCoches[0] = car1;
			ArrayCoches[1] = car2;
			ArrayCoches[2] = car3;
			ArrayCoches[3] = car4;
			
			for (Coches TotalCoches : ArrayCoches) {
								
				caracteristica(TotalCoches);
			}										
		}
	}
